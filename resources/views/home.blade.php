@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                @if($role=='admin')
                <h2>Customer Address</h2>

                    @foreach($customeraddress as $c)
                    <h5>{{ \App\User::find($c->customer_id)->name }}</h5><br>
                      <p> {{ $c->address }} </p>
                      <hr/>
                    @endforeach
                @else

                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
