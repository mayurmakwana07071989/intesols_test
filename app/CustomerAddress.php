<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
	protected $table = 'customer_address';
     protected $fillable = [
        'customer_id','address','is_primary'
    ];
}
