<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerAddress;
use Auth;
class CustomerApiController extends Controller
{
    public function customerlist(){
    	$token = \JWTAuth::getToken();
    	$user = \JWTAuth::toUser($token);

		if($user->role=='admin')
		{
    		return response()->json(['status'=>'success','data' => CustomerAddress::get()], 200);
    	}else{
    		return response()->json(['status'=>'failed','message'=>'Unauthorized Access'], 403);
    	}
    }
}
